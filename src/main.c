#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define WORDMAXLEN 100

// Defines data to store in tree.
    typedef char data_type[WORDMAXLEN];

// Defines node and its pointer.
    typedef struct bstnd {
        data_type data;         //Word
        int freq;               //Frequency
        struct bstnd* parent;
        struct bstnd* left;
        struct bstnd* right;
    } bstnode, *pbstnode;

// Defines the tree.
    typedef struct bt {
        pbstnode root;
    } bst;

// Compare function for quicksort.
    static int compare(const void *p1, const void *p2);

// Traverse tree and store nodes into array.
    int storeinarray(pbstnode tree,pbstnode * array,int index);

// Allocate node with data and insert in the tree. If data already in tree increase frequency.
    int bst_insert(bst *bt, data_type key);

// Delete the whole tree of given root node.
    void bst_delete(pbstnode node);

int main( int argc, char* argv[] ){

    clock_t tik = clock();

	bst tree;
	tree.root=0;

    FILE * fp;
    int i=0, c=0, s=0, x=0;
    char buffer[WORDMAXLEN]={'\0'};
    fp = fopen(argv[1], "r");

    // If no argument given print correct usage and exit.
    if (fp==0) {
        printf("Usage: %s [file]\n", argv[0]);
        return 0;
    }
    // Loop until end of file. This loop will add all the words from the text file to the binary search tree.
    while (!feof(fp)) {
        c = fgetc(fp);

        // If c is alphabetic or apostrophe write to buffer[index] and increment index.
        if (isalpha(c) || c == '\'' || c == '�' || c == '�') {
            buffer[i]=toupper(c);
            i++;
            // If word over max length found, increment x as a warning flag and stay at the index until non-alphabet.
            if (i>=WORDMAXLEN) {
                x++;
                i--;
            }
        }
        // Write null terminator to array when no more alphabetics found.
        else {
            buffer[i]='\0';

            // If first char in buffer is alphabetic and insert() returns 1, increment number of tree elements (words).
            if (isalpha(buffer[0]) && bst_insert( &tree, buffer )) {
                s++;
            }
            // Write null terminator to buffer[0] and reset index for next iteration.
            buffer[0]='\0';
            i=0;
        }
    } // End tree insertion loop.

    // Allocate memory for an array of node pointers with size (# of tree elements) * (node size).
    pbstnode * ndlist = (pbstnode *)malloc(s * sizeof(pbstnode));

    // Store tree elements into array and quick sort them according to frequency.
    storeinarray(tree.root,ndlist,0);
    qsort(ndlist, s, sizeof(pbstnode), compare);

    printf("Number of different words = %d\nThe 100 most common words:\nWORD NUMBER OF OCCURRENCES\n", s);

    // Print first 100 or all if less word/frequency pairs from sorted array.
    for (i=0; i<s; i++) {
        if (i==100) {
            break;
        }
        printf("%s %d\n", ndlist[i]->data,ndlist[i]->freq);
    }
    // Delete the binary search tree from root.
    bst_delete(tree.root);

    // If words over the max length found, print warning.
    if (x) {printf("\n!!!WARNING!!! One or more over maximum %d-character words shortened!\n", WORDMAXLEN);}

    clock_t tok = clock();

    printf("\nELAPSED: %f SECONDS\n", (double)(tok - tik) / CLOCKS_PER_SEC);

	return 0;
}

int storeinarray(pbstnode tree,pbstnode * array,int index) {
    if (tree != 0){
        // Stores recursively all the elements on the left.
        index = storeinarray(tree->left, array, index);
        // The root of the (sub)tree.
        array[index] = tree;
        // Stores recursively all the elements on the right.
        return storeinarray(tree->right, array, index + 1);
    }
    return index;
}

static int compare(const void *p1, const void *p2) {
    const bstnode *node1 = *(const pbstnode *)p1;
    const bstnode *node2 = *(const pbstnode *)p2;
    // Return negative of value for descending order.
    return -(node1->freq - node2->freq);
}

int bst_insert(bst *bt, data_type key){
	bstnode *n = (bstnode *)malloc(sizeof(bstnode));
	n->freq=1;
	strcpy(n->data, key);
	n->parent=n->right=n->left=0;
	pbstnode x = bt->root;
	pbstnode y=0;
	while(x!=0){
		y=x;
                // Strcmp() for comparing data.
		if  (strcmp( key, x->data ) < 0 )
			x = x->left;
		else if (strcmp( key, x->data ) > 0 )
			x = x->right;
		else if (strcmp( key, x->data ) == 0 ) {
            x->freq++;
            // Return 0 if data already in tree.
            return 0;
		}
	}

	n->parent = y;
	if (y == 0)
		bt->root = n;
	else if (strcmp( key, y->data ) < 0 )
		y->left = n;
	else
		y->right = n;
// Return 1 if new element added.
return 1;
}

void bst_delete(pbstnode node){
	if(node != 0) {
		bst_delete(node->left);
		bst_delete(node->right);
		free(node);
	}
}
