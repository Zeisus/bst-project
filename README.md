# BST-project

This is a program written in C that takes a single text file as a parameter and then counts the occurences of different words. Finally the program sorts the top 100 used words with occurence counts and prints them.

The program implements a binary search tree data structure that is used to store the words and their occurences. Quicksort is used to sort the words according to occurence count.